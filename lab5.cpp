/**********************
 *Jake Liguori
 *jrliguo
 *Lab 5
 *Lab Section: 001
 *TA: Nushrat Humaira
 **********************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}

int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  Card deck[52];
  // Intializes deck. 4 is our # of suits,
  // and 13 is # of cards per suit
  for (int i = 0, j = 0; i < 4; i++, j += 13) {
    for (int k = 0; k < 13; k++) {
      deck[j+k].suit = static_cast<Suit>(i);
      deck[j+k].value = k + 2;
    }
  }
  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
  random_shuffle(&deck[0], &deck[52], myrandom);
   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
  Card hand[] = {deck[0], deck[1], deck[2], deck[3], deck[4]};
  sort(hand, hand + 5, suit_order);
  // Prints out the whole hand to the terminal.
  for (int i = 0; i < 5; i++) {
    cout << setw(10) << right << get_card_name(hand[i]) << " of "
    << get_suit_code(hand[i]) << endl;
  }

  return 0;
}

// Sorts two cards by suit. If same suit, the lower card comes first
bool suit_order(const Card& lhs, const Card& rhs) {
  if (lhs.suit < rhs.suit) {
    return true;
  } else if (lhs.suit == rhs.suit) {
    if (lhs.value < rhs.value) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}
// Retrieves the string for the suit of the card
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}
// Retrieves the string for the value of the card
string get_card_name(Card& c) {
  switch (c.value) {
    case 2:         return "2";
    case 3:         return "3";
    case 4:         return "4";
    case 5:         return "5";
    case 6:         return "6";
    case 7:         return "7";
    case 8:         return "8";
    case 9:         return "9";
    case 10:        return "10";
    case 11:        return "Jack";
    case 12:        return "Queen";
    case 13:        return "King";
    case 14:        return "Ace";
    default:        return " ";
  }
}
